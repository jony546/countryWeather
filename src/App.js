import React from 'react';
import qwest from "qwest";
import './App.css';
import { Provider as ReduxProvider } from "react-redux";
import configureStore from "./modules/store";
import WeatherMap from './components/WeatherMap';
const reduxStore = configureStore(window.REDUX_INITIAL_DATA);

qwest.setDefaultOptions({
  dataType: 'json',
  cache: true,
  headers: {
    'Access-Control-Allow-Origin': '*'
  }
});


function App() {
  return (
    <ReduxProvider store={reduxStore}>
      <div className="App">
        <WeatherMap />
      </div>
    </ReduxProvider>
  );
}

export default App;
