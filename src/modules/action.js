import ApiCall from '../ApiCall/ApiCall';
import _ from "lodash";

// types of action
const Types = {
  GET_WEATHER: "GET_WEATHER",
  TOGGLE_VISIBILITY_WEATHER_INFO: "TOGGLE_VISIBILITY_WEATHER_INFO",
  GET_CITIES: "GET_CITIES"
};
// actions


export function GetWeather(lat, lng, city) {
  return (dispatch, getState) => {
    ApiCall(getState().ApiUrl + "getWeather?" + `lat=${lat}&lng=${lng}&city=${city}`).then(weatherData => {
      dispatch({ type: Types.GET_WEATHER, weatherData });
    }).catch((error) => {
      alert("Error: " + error.toString());
      dispatch({ type: Types.GET_WEATHER, weatherData: {} });
    })
  }
}

export function GetCities(lat, lng) {
  return (dispatch, getState) => {
    dispatch({ type: Types.GET_WEATHER, weatherData: {} });
    ApiCall(getState().ApiUrl + "getCities?" + `lat=${lat}&lng=${lng}`).then(addreses => {
      var cities = [];
      _.forEach(addreses.results, (value, key) => {
        _.forEach(value.address_components, (v, k) => {
          if (!_.find(cities, (e) => { return v.short_name == e.value && !_.isNumber(e.name) })) {
            cities.push({ name: v.long_name, value: v.short_name });
          }
        });
      });
      dispatch({ type: Types.GET_CITIES, cities });
    }).catch((error) => {
      alert("Error: " + error.toString());
      dispatch({ type: Types.GET_CITIES, cities: [] });
    })
  }
}

export function ToggleWeatherInfo() {
  return (dispatch, getState) => {
    dispatch({ type: Types.TOGGLE_VISIBILITY_WEATHER_INFO, openWeatherInfo: !getState().openWeatherInfo });
  }
}

export default {
  GetWeather,
  GetCities,
  ToggleWeatherInfo,
  Types
};