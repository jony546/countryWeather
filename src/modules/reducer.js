import ACTIONS from "./action";
import _ from "lodash";

const defaultState = {
    weatherData: {},
    ApiUrl: "http://localhost:3001/",
    cities: []
};

const todoReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.GET_WEATHER: {
            let newState = _.cloneDeep(state);
            newState.weatherData = action.weatherData;
            return newState;
        }

        case ACTIONS.Types.TOGGLE_VISIBILITY_WEATHER_INFO: {
            let newState = _.cloneDeep(state);
            newState.openWeatherInfo = action.openWeatherInfo;
            return newState;
        }

        case ACTIONS.Types.GET_CITIES: {
            let newState = _.cloneDeep(state);
            newState.cities = action.cities;
            return newState;
        }

        default:
            return state;
    }
};

export default todoReducer;