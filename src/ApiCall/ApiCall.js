import qwest from 'qwest';

function Call(url, data = null) {
    var method = data ? 'post' : 'get';
    var xhr = qwest[method](url, data)
        .then((xhr, response) => {
            return response
        })
        .catch((error, xhr, response) => {
            response = JSON.parse(response);
            return Promise.reject(response.Mensaje ? response.Mensaje : response.Message ? response.Message : error)

        })
    return xhr;
};

export default Call;