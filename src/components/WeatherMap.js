import React from 'react';
import ReactDOM from 'react-dom';
import ACTIONS from "../modules/action";
import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, Row, Col, Form } from 'react-bootstrap';
const api_key = "AIzaSyC7rgzoXPzL2JECQ8zE_69mTAOWLUNxG0o";

class WeatherMap extends React.Component {
    constructor() {
        super();
        this.state = {
            zoom: 3,
            loaded: false,
            center: { lat: 47.444, lng: -122.176 },
            marcadorCentral: false,
            infoWindow: null,
            cargando: false,
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
            markerPosition: null,
            markers: []
        }
    }

    mapClick = (mapProps, map, clickEvent) => {
        const lat = clickEvent.latLng.lat();
        const lng = clickEvent.latLng.lng();
        var markers = this.state.markers;
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        var marker = new window.google.maps.Marker({
            position: { lat, lng },
            map: map
        });
        markers.push(marker);

        this.setState({ markerPosition: { lat, lng }, activeMarker: marker, showingInfoWindow: true, markers });
        this.props.GetCities(lat, lng);
    }

    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });
    }

    render() {
        var _this = this;
        setInterval(function () {
            if (document.getElementById("citySelect") && _this.props.cities.length) {
                var citySelect = (
                    <Form.Control as="select" onChange={(evt) => {
                        var position = _this.state.markerPosition;
                        _this.props.GetWeather(position.lat, position.lng, evt.target.value)
                    }}>
                        <option value="">--Seleccione--</option>
                        {
                            _this.props.cities.map((item, index) => {
                                return (<option key={index} value={item.value}>{item.name}</option>)
                            })
                        }
                    </Form.Control>
                );
                ReactDOM.render(React.Children.only(citySelect), document.getElementById("citySelect"))
            }
        }, 100);

        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={this.state.zoom}
                    maxZoom={this.state.zoom}
                    minZoom={this.state.zoom}
                    onClick={this.mapClick}
                    style={{ width: '100%', height: '100%' }}
                    initialCenter={this.state.center}
                >
                    {
                        this.state.markerPosition ?
                            <Marker onClick={this.onMarkerClick}
                                name={'Current location'} position={this.state.markerPosition} />
                            : null
                    }
                    {
                        this.state.activeMarker ?
                            <InfoWindow
                                marker={this.state.activeMarker}
                                visible={this.state.showingInfoWindow}>
                                <div>
                                    {
                                        this.props.weatherData.hasOwnProperty('timezone') ?
                                            <Card>
                                                <Card.Header>{this.props.weatherData.timezone}</Card.Header>
                                                <Card.Body>
                                                    <Row>
                                                        <Col>
                                                            Temperatura Farenheit: {this.props.weatherData.currently ? this.props.weatherData.currently.temperature + "°F" : ""}
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            Temperatura Celsius: {this.props.weatherData.currently ? ((this.props.weatherData.currently.temperature - 32) * 5 / 9).toFixed(2) + "°C" : ""}
                                                        </Col>
                                                    </Row>
                                                </Card.Body>
                                            </Card>
                                            : this.props.cities.length ?
                                                <Card>
                                                    <Card.Header><h1>Ciudades</h1></Card.Header>
                                                    <Card.Body>
                                                        <Row style={{ marginTop: "0.3em" }}>
                                                            <Col>
                                                                <Form.Group controlId="citiesGroup">
                                                                    <div>
                                                                        <Form.Label>Seleccione Ciudad</Form.Label>
                                                                    </div>
                                                                    <div id="citySelect">

                                                                    </div>
                                                                </Form.Group>

                                                            </Col>
                                                        </Row>
                                                    </Card.Body>
                                                </Card>
                                                : <h1>No hay Información</h1>
                                    }
                                </div>
                            </InfoWindow>
                            : null
                    }
                </Map>
            </div>
        );
    }
}

function mapStore(store) {
    return {
        googleApikey: store.googleApikey,
        weatherData: store.weatherData,
        cities: store.cities
    }
}
function mapActions(dispatch) {
    return bindActionCreators({ ...ACTIONS }, dispatch);
}

export default connect(mapStore, mapActions)(GoogleApiWrapper({
    apiKey: api_key
})(WeatherMap))
